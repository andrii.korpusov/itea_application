<?php
use Psr\Container\ContainerInterface;

return [
    'ArticleManager' => function(ContainerInterface $c) {
        return new \AndriiKorpusov\IteaProject\Blog\Service\ArticleManager($c->get('Repository'), $c->get('ArticleTransformer'));
    },
    'Repository' => \DI\factory(function(ContainerInterface $container) {
        return new \AndriiKorpusov\IteaProject\Blog\Repository\Repository($container->get(PDO::class));
    }),
    'ArticleTransformer' => function() {
        return new \AndriiKorpusov\IteaProject\Blog\Transformer\ArticleTransformer();
    },
    'ArticleController' => function(ContainerInterface $c) {
        return new \AndriiKorpusov\IteaProject\Blog\Controller\ArticleController($c);
    },
    'DefaultController' => function(ContainerInterface $c) {
        return new \AndriiKorpusov\IteaProject\Blog\Controller\DefaultController($c);
    },
    PDO::class => function (ContainerInterface $c) {
        $db = $c->get('DB');
        return new PDO($db['host'], $db['username'], $db['password']);
    }
];