<?php
namespace AndriiKorpusov\IteaProject;

use Psr\Log\LoggerInterface;

class CSVCreator
{
    private $logger;

    private $dir;

    public function __construct(LoggerInterface $logger, $dir)
    {
        $this->logger = $logger;

        $this->dir = $dir;
    }

    public function createCsv($array)
    {
        $filename = sprintf('%s/%s.csv', $this->dir, uniqid());

        try {
            $stream = fopen($filename, 'w+');

            foreach ($array as $item) {
                fputcsv($stream, (array)$item);
            }

            fclose($stream);

        } catch (\Throwable $e) {
            $this->logger->error($e->getMessage());
            unlink($filename);
        }


        return $filename;
    }
}