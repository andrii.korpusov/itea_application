<?php

namespace AndriiKorpusov\IteaProject\Factory;

class TheatreFinder
{
    public function getHall(): HallInterface
    {
        return new Theatre();
    }
}