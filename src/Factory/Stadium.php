<?php

namespace AndriiKorpusov\IteaProject\Factory;

class Stadium implements HallInterface
{

    public function getFreePlaces()
    {
        return [
            'type' => 'Stadium'
        ];
    }
}