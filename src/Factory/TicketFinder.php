<?php
namespace AndriiKorpusov\IteaProject\Factory;

abstract class TicketFinder
{

    abstract public function getHall(): HallInterface;


    //Вся логіка пошуку зводиться до цього єдиного методу
    // метод не знає з яким саме типом Hall йому потрібно працювати, ми лише знаємо що наш Hall імплементує HallInterface
    public function getTicketPlaces()
    {
        //Отримуємо HallInterface
        $hall = $this->getHall();


        return $hall->getFreePlaces();
    }

}