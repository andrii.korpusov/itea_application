<?php

namespace AndriiKorpusov\IteaProject\Factory;


class Theatre implements HallInterface
{
    public function getFreePlaces()
    {
        return [
            'type' => 'Theatre'
        ];
    }
}