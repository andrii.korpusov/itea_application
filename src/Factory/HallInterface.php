<?php

namespace AndriiKorpusov\IteaProject\Factory;

interface HallInterface
{
    public function getFreePlaces();
}