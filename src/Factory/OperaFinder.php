<?php

namespace AndriiKorpusov\IteaProject\Factory;

class OperaFinder extends TicketFinder
{
    public function getHall(): HallInterface
    {
        return new Opera();
    }
}