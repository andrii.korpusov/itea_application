<?php

namespace AndriiKorpusov\IteaProject\Factory;

class Opera implements HallInterface
{
    public function getFreePlaces()
    {
        return [
            'type' => 'Opera'
        ];
    }
}