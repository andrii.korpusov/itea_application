<?php

namespace AndriiKorpusov\IteaProject\Cor;

abstract class ChainBaseClass
{
    private $nextChain;

    //Встановлюємо наступний обробник
    public function setNext(ChainBaseClass $nextChain): ChainBaseClass
    {
        $this->nextChain = $nextChain;

        //Повертаємо наступний обробник
        return $nextChain;
    }

    public function check($arg)
    {
        //якщо це останній обробник і наступного немає - повертаєм потрібни нам результат (true|false|Exception) згідно логіки
        if (!$this->nextChain) {
            return false;
        }

        //Викликаємо метод check з наступного обробника
        return $this->nextChain->check($arg);
    }
}