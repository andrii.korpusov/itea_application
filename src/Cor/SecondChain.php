<?php

namespace AndriiKorpusov\IteaProject\Cor;

class SecondChain extends ChainBaseClass
{

    public function check($arg)
    {
        if ($arg < 4) {
            return __CLASS__;
        }

        return parent::check($arg);
    }
}