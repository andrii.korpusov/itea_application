<?php

namespace AndriiKorpusov\IteaProject\Cor;

class ThirdChain extends ChainBaseClass
{
    public function check($arg)
    {
        if ($arg < 5) {
            return __CLASS__;
        }

        return parent::check($arg);
    }
}