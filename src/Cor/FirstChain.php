<?php
namespace AndriiKorpusov\IteaProject\Cor;

class FirstChain extends ChainBaseClass
{

    public function check($arg)
    {
        if ($arg < 2) {
            return __CLASS__;
        }

        return parent::check($arg);
    }
}