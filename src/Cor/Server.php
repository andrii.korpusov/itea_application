<?php

namespace AndriiKorpusov\IteaProject\Cor;

class Server
{
    private $chain;

    public function setChain(ChainBaseClass $chain)
    {
        $this->chain = $chain;
    }

    public function runCheck($arg)
    {
        //Першим викликається наш FirstChain
        if ($result = $this->chain->check($arg)) {
            return 'Check ' . $result;
        }

        return 'no check';
    }
}