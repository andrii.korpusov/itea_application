<?php

namespace AndriiKorpusov\IteaProject\Blog\Controller;

use \AndriiKorpusov\IteaProject\Blog\Service\TagManager;

class TagController extends BaseController
{
    private $tagManager;

    public function __construct()
    {
        parent::__construct();

        $this->tagManager = new TagManager($this->repository);
    }

    public function tag($id)
    {
        $tag = $this->tagManager->findOneById($id);

        echo json_encode($this->tagManager->getTransformedTagData($tag));
    }

    public function tags()
    {
        $tags = $this->tagManager->findAll();

        $result = [];

        foreach ($tags as $tag) {
            $result[] = $this->tagManager->getTransformedTagData($tag);
        }

        echo json_encode($result);
    }

    public function create()
    {
        $tag = $this->tagManager->create($this->parseInputData('tag'));

        echo json_encode([
            "data" => [
                "id" => $tag->getId(),
                "name" => $tag->getName(),
                "active" => $tag->isActive(),
            ]]);
    }

    public function update($id)
    {
        $tag = $this->tagManager->findOneById($id);

        $this->tagManager->update($tag, $this->parseInputData('tag'));

        echo json_encode([
            "data" => [
                "id" => $tag->getId(),
                "name" => $tag->getName(),
                "active" => $tag->isActive(),
            ]]);
    }

    public function delete($id)
    {
        $deleted = $this->tagManager->delete($id);

        echo json_encode([
            "data" => [
                "deleted" => $deleted,

            ]]);
    }
}