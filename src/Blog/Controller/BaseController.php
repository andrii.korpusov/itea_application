<?php

namespace AndriiKorpusov\IteaProject\Blog\Controller;

use AndriiKorpusov\IteaProject\Blog\Repository\Repository;
use Psr\Container\ContainerInterface;
use Twig\Environment;
use Twig\Extension\DebugExtension;
use Twig\Loader\FilesystemLoader;

class BaseController
{
    private $twig;

    protected $container;


    public function __construct(ContainerInterface $container)
    {
        $baseDir = $_SERVER['DOCUMENT_ROOT'];

        $this->container = $container;

        $loader = new FilesystemLoader(sprintf('%s/templates', $baseDir));

        $this->twig = new Environment($loader, [
            'debug' => true
//            'cache' => sprintf('%s/cache', $baseDir)
        ]);

        $this->twig->addExtension(new DebugExtension());
    }

    public function render($template, $options = [])
    {
        return $this->twig->render($template, $options);
    }

    protected function parseInputData($name)
    {
        $input = file_get_contents('php://input');

        $result = json_decode($input);

        if ($result === NULL && !isset($result->$name)) {
            throw new BadRequestException('Cant find data in request');
        }

        return $result->$name;
    }
}