<?php

namespace AndriiKorpusov\IteaProject\Blog\Controller;

use AndriiKorpusov\IteaProject\Blog\Service\DirectoryFilterIterator;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGenerator;

class DefaultController extends BaseController
{
    /**
     * @return
     */
    public function index()
    {

    }

    /**
     * @Route()
     */
    public function login()
    {
        /**
         * Buckets
         * HashTable
         *
         * 1. link first element
         * 2. link last element
         * 3. link current element
         * 4. link buckets element
         * 5. buckets storage
         *
         *
         * Списки
         * 1. Однозв'язні link to next element
         * 2. Двозвязні link to prev&next element
         *
         *
         * Дерева
         * Nested Sets
         * Купа Heap
         *
         * Exception
         *
         *
         *
         *
         */
    }
}