<?php

namespace AndriiKorpusov\IteaProject\Blog\Controller;

use AndriiKorpusov\IteaProject\Blog\Service\ArticleManager;
use Psr\Container\ContainerInterface;

class ArticleController extends BaseController
{
    private $articleManager;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);

        $this->articleManager = $this->container->get('ArticleManager');
    }

    public function article($id)
    {
        $article = $this->articleManager->findOneById($id);

        echo json_encode($this->articleManager->getTransformedData($article));
    }

    public function articles()
    {
        $articles = $this->articleManager->findAll();

        $result = [];

        foreach ($articles as $article) {
            $result[] = $this->articleManager->getTransformedData($article);
        }

        echo json_encode($result);

    }

    public function create()
    {

        $input = file_get_contents('php://input');
        $params = json_decode($input);


        if (!isset($params->article)) {
            echo json_encode([
                'error' => true
            ]);
            exit;
        }

        $article = $this->articleManager->create($params->article);

        echo json_encode([
                "data" => [
                    "id" => $article->getId(),
                    "title" => $article->getTitle(),
                    "text" => $article->getText(),
                    "createdAt" => $article->getCreatedAt(),
                    "userId" => $article->getUserId()
                ]]);
    }

    public function update($id)
    {
        echo json_encode([$id]);
    }

    public function delete($id)
    {
        echo json_encode(true);
    }


}