<?php
namespace AndriiKorpusov\IteaProject\Blog\Repository\RepositoryHelper;

class SQLConstructor
{
    private $excludeMethods = [
        'getId',
        'setId'
    ];

    public function getProperties($object, $update = false): array
    {
        $column = [];
        $value = [];

        foreach (get_class_methods($object) as $method) {
            if (strpos($method, 'set') !== false && !in_array($method, $this->excludeMethods)) {
                $property = lcfirst(str_replace('set', '', $method));

                $getterName = str_replace('set', 'get', $method);
                $getIsName = str_replace('set', 'is', $method);

                if ((method_exists($object, $getterName) && $object->{$getterName}() !== null) || (method_exists($object, $getIsName) && $object->{$getIsName}() !== null)) {
                    $column[] = sprintf('`%s`', $property);
                    $value[] = '?';
                }
            }
        }

        return [
                'column' => $update ? $column : implode(',', $column),
                'value' => $update ? $value : implode(',', $value)
            ];
    }

    public function getParams($object): array
    {
        $result = [];
        foreach (get_class_methods($object) as $method) {
            if (in_array($method, $this->excludeMethods)) {
                continue;
            }
            if(strpos($method, 'get') !== false || strpos($method, 'is') !== false) {
                if ($object->{$method}() !== null) {
                    $result[] = $object->{$method}();
                }
            }
        }

        return $result;
    }

    public function getParamsTypes($params): string
    {
        $result = '';

        foreach ($params as $param) {
            switch (gettype($param)) {
                case 'integer':
                case 'boolean':
                    $result .= 'i';
                    break;
                case 'string':
                default:
                    $result .= 's';
                    break;
            }
        }

        return $result;
    }

//    public function getUserId():string
//    {
//        $userId='';
//    }
}