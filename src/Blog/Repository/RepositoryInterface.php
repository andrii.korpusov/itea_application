<?php

namespace AndriiKorpusov\IteaProject\Blog\Repository;

use AndriiKorpusov\IteaProject\Blog\Entity\EntityInterface;

interface RepositoryInterface
{
    public function insert(EntityInterface $object);

    public function update(EntityInterface $object);

    public function delete(EntityInterface $object);
}