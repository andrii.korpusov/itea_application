<?php
namespace AndriiKorpusov\IteaProject\Blog\Repository;

use AndriiKorpusov\IteaProject\Blog\Entity\EntityInterface;
use AndriiKorpusov\IteaProject\Blog\Repository\RepositoryHelper\SQLConstructor;
use AndriiKorpusov\IteaProject\Blog\Service\DocsReader;

/**
 * @DBTableName(name=db)
 */

class Repository implements RepositoryInterface
{
    private $connection;

    private static $self;

    private $docsReader;

    private $sqlConstructor;

    public function __construct(\PDO $connection)
    {
        $this->connection = $connection;
        $this->docsReader = new DocsReader();
        $this->sqlConstructor = new SQLConstructor();
    }

//    private function __clone()
//    {}

//    public function __wakeup()
//    {
//        throw new \Exception('Cant wake up singleton class');
/*
 * 1. set balance-50 user 1
 * 2. set balance+50 user 2
 *
 *
 * ACID
 *
 * Atomicity - атомарнсть гарантує що транзакція не може виконатися частково
 *
 * Consistency - узгодженість - не можемо зафіксувати некоректні дані
 *
 * Isolation - ізольованість - паралельні транзакції не маю впливати на результат виконання інших транзакцій
 *
 * Durability - стійкість - якщо ми отримали резуальтат що все збережено
 *
 *
 * Read Uncommited - дозволяє читати зміни з інших незавершених транзакцій - рівень ізоляції  - 0
 *
 * Read Commited - дозволяє читати зміни лише зафіксованих транзакцій (commit)
 *
 * Repeatable read - не видно змін в Update Delete
 *
 * Serializable - самий жорсткий, складний тип транзакцій
 *
 *
 *
 *
 *
 */

//    }

//    public static function getInstance($config)
//    {
//        if (!self::$self instanceof Repository) {
//            $repository = new Repository();
//            $repository->connection = mysqli_connect('localhost', $config['user'], $config['pass'], $config['db']);
//            $repository->docsReader = new DocsReader();
//            $repository->sqlConstructor = new SQLConstructor();
//
//            self::$self = $repository;
//        }
//
//        return self::$self;
//    }

    public function insert($object)
    {
        $properties = $this->sqlConstructor->getProperties($object);

        $params = $this->sqlConstructor->getParams($object);

        $types = $this->sqlConstructor->getParamsTypes($params);

        try {
            $sql = sprintf("INSERT into %s (%s) VALUES (%s)", $this->docsReader->getTableName($object), $properties['column'], $properties['value']);

            $stmt = $this->connection->prepare($sql);

            $i = 1;
            foreach ($params as $param) {
                $stmt->bindParam($i, $param);

                if ($i===count($params)) {
                    break;
                }
                $i++;
            }

            $stmt->execute();

            return $this->connection->lastInsertId();
        } catch (\Throwable $e) {
            var_dump($this->connection->errorInfo());
            echo $e->getMessage();
            exit;
        }

    }

    public function update($object): bool
    {
        try {
            $properties = $this->sqlConstructor->getProperties($object, true);

            $params = $this->sqlConstructor->getParams($object);

            $types = $this->sqlConstructor->getParamsTypes($params);

            $setParams = [];

            foreach ($properties['column'] as $k=>$property) {
                $setParams[] = sprintf('%s=%s', $property, $properties['value'][$k]);
            }
            $sqlUpdate = sprintf("UPDATE %s SET %s WHERE id=?", $this->docsReader->getTableName($object),implode(',', $setParams));

            $stmt = mysqli_prepare($this->connection, $sqlUpdate);

            $types .= 'i';
            $params[] = $object->getId();

            mysqli_stmt_bind_param($stmt, $types, ...$params);

            return mysqli_stmt_execute($stmt);
        } catch (\Throwable $e) {
            var_dump(mysqli_error($this->connection));
            echo $e->getMessage();
        }

        return false;
    }

    public function delete(EntityInterface $object)
    {
        $sqlDelete = sprintf("DELETE FROM %s WHERE id=:id",$this->docsReader->getTableName($object));
        $stmt = $this->connection->prepare($sqlDelete);

        $id = $object->getId();

        $stmt->bindParam('id', $id);

        return $stmt->execute();
    }

    public function findOneById(int $id, $className)
    {
        $sql = sprintf("SELECT * FROM %s WHERE id=:id", $this->docsReader->getTableName($className));
        $stmt = $this->connection->prepare($sql);
        $stmt->bindParam( 'id', $id);
        $stmt->execute();

        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }

    public function findAll($className)
    {
        $sql = sprintf("SELECT * FROM %s", $this->docsReader->getTableName($className));

        $stmt = $this->connection->query($sql);

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getConnection()
    {
        return $this->connection;
    }
}