<?php

namespace AndriiKorpusov\IteaProject\Blog\Routes;

use AndriiKorpusov\IteaProject\Blog\Controller\ArticleController;
use AndriiKorpusov\IteaProject\Blog\Controller\DefaultController;
use AndriiKorpusov\IteaProject\Blog\Controller\TagController;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

class Routes
{
    public function getRoutes(): RouteCollection
    {
        $route = new Route('/', ['_controller' => 'DefaultController', '_method' => 'index']);

        $routes = new RouteCollection();
        $routes->add('index', $route);
        $routes->add('login', new Route('/login', ['_controller' => 'DefaultController', '_method' => 'login']));

        $routes->add('blog_articles', new Route('/blog/articles', ['_controller' => 'ArticleController', '_method' => 'articles'],[],[],null, [], 'GET'));
        $routes->add('blog_article_create', new Route('/blog/articles/create', ['_controller' => 'ArticleController', '_method' => 'create'],[],[],null, [], 'PUT'));
        $routes->add('blog_article_find', new Route('/blog/articles/{id}', ['_controller' => 'ArticleController', '_method' => 'article'],[
            'id' => '[0-9]+'
        ],[],null, [], 'GET'));
        $routes->add('blog_article_update', new Route('/blog/articles/{id}/update', ['_controller' => 'ArticleController', '_method' => 'update'],[],[],null, [], 'POST'));
        $routes->add('blog_article_delete', new Route('/blog/articles/{id}/delete', ['_controller' => 'ArticleController', '_method' => 'delete'],[],[],null, [], 'DELETE'));

        $routes->add('blog_tags', new Route('/blog/tag', ['_controller' => TagController::class, '_method' => 'tag'],[],[],null, [], 'GET'));
        $routes->add('blog_tag_create', new Route('/blog/tag/create', ['_controller' => TagController::class, '_method' => 'create'],[],[],null, [], 'PUT'));
        $routes->add('blog_tag_find', new Route('/blog/tag/{id}', ['_controller' => TagController::class, '_method' => 'article'],[
            'id' => '[0-9]+'
        ],[],null, [], 'GET'));
        $routes->add('blog_tag_update', new Route('/blog/tag/{id}/update', ['_controller' => TagController::class, '_method' => 'update'],[],[],null, [], 'POST'));
        $routes->add('blog_tag_delete', new Route('/blog/tag/{id}/delete', ['_controller' => TagController::class, '_method' => 'delete'],[],[],null, [], 'DELETE'));

        return $routes;
    }
}