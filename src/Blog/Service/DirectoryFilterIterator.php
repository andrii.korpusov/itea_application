<?php

namespace AndriiKorpusov\IteaProject\Blog\Service;

class DirectoryFilterIterator extends \FilterIterator
{
    private $alloweExtensions = [
        'php'
    ];

    public function accept()
    {
        $current = $this->getInnerIterator()->current();

        return in_array(pathinfo($current->getFileName(), PATHINFO_EXTENSION), $this->alloweExtensions);
    }
}