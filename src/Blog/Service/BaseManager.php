<?php

namespace AndriiKorpusov\IteaProject\Blog\Service;

use AndriiKorpusov\IteaProject\Blog\Repository\Repository;

class BaseManager
{
    private $config = [
        'user' => 'root',
        'pass' => '',
        'db' => 'itea_project'
    ];

    protected $entityClass;

    protected $repository;

    private $dataMapper;

    public function __construct($repository)
    {

        $this->repository = $repository;

        $this->dataMapper = new DataMapper(new $this->entityClass);
    }


    public function findOneById(int $id)
    {
        $data = $this->repository->findOneById($id, $this->entityClass);

        return $this->dataMapper->getObjectFromData($data, $this->entityClass);
    }

    public function findAll()
    {
        $entities = $this->repository->findAll($this->entityClass);

        $result = [];

        foreach ($entities as $entity) {
            $result[] = $this->dataMapper->getObjectFromData($entity, $this->entityClass);
        }

        return $result;
    }


}