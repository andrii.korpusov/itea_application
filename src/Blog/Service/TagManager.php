<?php

namespace AndriiKorpusov\IteaProject\Blog\Service;

use AndriiKorpusov\IteaProject\Blog\Entity\Tag;
use AndriiKorpusov\IteaProject\Blog\Repository\Repository;
use AndriiKorpusov\IteaProject\Blog\Repository\RepositoryInterface;

class TagManager extends BaseManager
{
    public function __construct($repository)
    {
        $this->entityClass = Tag::class;

        parent::__construct($repository);

    }
    public $tagDataTransformer;

    public function create(object $data)
    {
        $tag = new Tag();

        $tag->setName($data->name)
            ->setActive(true);

        try {
            $id = $this->repository->insert($tag);
            $tag->setId($id);
            return $tag;
        } catch (\Throwable $e) {
            echo $e->getMessage();
        }


    }

    public function delete(int $id)
    {
        $tag = $this->findOneById($id);

        return $this->repository->delete($tag);
    }


    public function update(Tag $tag, object $data)
    {
        $tag->setName($data->name)
            ->setActive(true);

        $this->repository->update($tag);
    }

    public function getTransformedTagData($entity)
    {
        return $this->tagDataTransformer->getTransformedTagData($entity);
    }
}