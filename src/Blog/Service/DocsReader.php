<?php

namespace AndriiKorpusov\IteaProject\Blog\Service;

use AndriiKorpusov\IteaProject\Blog\Repository\Repository;
class DocsReader
{

    public function getTableName($class)
    {
        try {
            $ReflectorClass = new \ReflectionClass($class);

            preg_match("/DBTableName\(name=(.+?)\)/", $ReflectorClass->getDocComment(),$matches);
            if (!$matches[1]) {
                throw new \Exception('cant find @DBTableName annotation');
            }
            $tableName= $matches[1];

            return $tableName;
        } catch (\Throwable $e) {
            echo $e->getMessage();
        }

    }
}