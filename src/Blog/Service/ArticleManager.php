<?php


namespace AndriiKorpusov\IteaProject\Blog\Service;


use AndriiKorpusov\IteaProject\Blog\Entity\Article;
use AndriiKorpusov\IteaProject\Blog\Repository\Repository;
use AndriiKorpusov\IteaProject\Blog\Transformer\ArticleTransformer;

class ArticleManager extends BaseManager
{
    public $dataTransformer;

    public function __construct(Repository $repository, ArticleTransformer $articleTransformer)
    {

        $this->entityClass = Article::class;

        $this->dataTransformer = $articleTransformer;

        parent::__construct($repository);
    }

    public function create(object $data)
    {
        $article = new Article();

        $article->setTitle($data->title)
            ->setText($data->text)
            ->setUserId($data->userId)
            ->setCreatedAt((new \DateTime())->format('Y-m-d H:i:s'))
            ->setUpdatedAt((new \DateTime())->format('Y-m-d H:i:s'))
            ->setActive(true);

        $id = $this->repository->insert($article);

//        $article->setId($id);

        return $article;
    }

    public function save($article, $new = false) {
        if ($new) {
            return $this->repository->insert($article);
        }

        return $this->repository->update($article);
    }

    public function update(Article $article, object $data)
    {
        $article->setTitle($data->title)
            ->setText($data->text)
            ->setUserId($data->userId)
            ->setUpdatedAt(new \DateTime())
            ->isActive(true);
    }

    public function delete(int $id)
    {
        echo "deleted";
    }

    public function getTransformedData($entity)
    {
        return $this->dataTransformer->getTransformedData($entity);
    }

}