<?php

namespace AndriiKorpusov\IteaProject\Blog\Service;


class DataMapper
{
    private $object;

    public function __construct($object)
    {
        $this->object = $object;
    }

    public function getDataFromObject()
    {

    }

    public function getObjectFromData($data)
    {
//        $object = new $className;

        foreach ($data as $key => $value) {
            $methodName = sprintf('set%s', ucfirst($key));

            if (method_exists($this->object, $methodName)) {
                $this->object->{$methodName}($value);
            }
        }

        return $this->object;
    }
}