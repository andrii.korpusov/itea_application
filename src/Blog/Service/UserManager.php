<?php

namespace AndriiKorpusov\IteaProject\Blog\Service;

use AndriiKorpusov\IteaProject\Blog\Entity\User;

class UserManager
{
    public function create(array $data)
    {
        $user = new User();

        $salt = uniqid();

        $user->setFirstName($data['firstName'])
            ->setLastName($data['lastName'])
            ->setEmail($data['email'])
            ->setPassword($this->getPassword($data['password'], $salt))
            ->setSalt($salt)
            ->setCreatedAt((new \DateTime())->format('Y-m-d H:i:s'))
            ->setActive(true)
            ->setRole($data['role']);

        return $user;
    }

    public function getPassword($password, $salt)
    {
        return md5(sprintf('%s.%s', $salt, $password));
    }
}