<?php

namespace AndriiKorpusov\IteaProject\Blog\Transformer;

abstract class BaseTransformer
{
    protected $data;

    protected $errors;

    abstract public function transform($entity);

    public function getTransformedData($entity)
    {
        $data = $this->transform($entity);

        if ($data) {

            return [
                'data' => $this->transform($entity),
            ];
        }

        return [
            'errors' => 'cant transform data'
        ];
    }
}