<?php
namespace AndriiKorpusov\IteaProject\Blog\Transformer;

use AndriiKorpusov\IteaProject\Blog\Entity\User;

class ArticleTransformer extends BaseTransformer
{
    public function transform($entity)
    {
        return [
            'type' => get_class($entity),
            'id' => $entity->getId(),
            'attributes' => [
                'title' => $entity->getTitle(),
                'text' => $entity->getText(),
                'createdAt' => $entity->getCreatedAt(),
                'active' => $entity->isActive() ? 'true' : 'false'
            ],
            'links' => [
                'self' => '',
                'related' => ''
            ],
            'relationships' => [
                'author' => [
                    'data' => [
                        'type' => User::class,
                        'id' => $entity->getUserId()
                    ],
                    'links' => [
                        'self' => ''
                    ]
                ]
            ]
        ];
    }
}