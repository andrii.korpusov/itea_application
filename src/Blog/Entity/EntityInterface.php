<?php

namespace AndriiKorpusov\IteaProject\Blog\Entity;

interface EntityInterface
{
    public function getId();
}