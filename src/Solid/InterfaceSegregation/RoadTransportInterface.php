<?php

namespace AndriiKorpusov\IteaProject\Solid\InterfaceSegregation;

interface RoadTransportInterface
{
    public function getWheelsCount();

    public function getTurnAngle();
}