<?php

namespace AndriiKorpusov\IteaProject\Solid\InterfaceSegregation;

interface RailwayTransportInterface
{
    public function getTrackWidth();
}