<?php

namespace AndriiKorpusov\IteaProject\Solid\InterfaceSegregation;

interface WaterTransportInterface
{
    public function getShipLength();
}