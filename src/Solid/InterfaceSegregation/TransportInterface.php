<?php
namespace AndriiKorpusov\IteaProject\Solid\InterfaceSegregation;

interface TransportInterface
{
    public function getWeight();

    public function getSpeed();
}