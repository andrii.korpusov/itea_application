<?php

namespace AndriiKorpusov\IteaProject\Solid\DependencyInversion;

class HttpRequest implements RequestInterface
{
    public function get($url)
    {
        $data = file_get_contents($url);

        return json_decode($data);
    }
}