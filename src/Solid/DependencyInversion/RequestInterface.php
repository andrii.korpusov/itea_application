<?php

namespace AndriiKorpusov\IteaProject\Solid\DependencyInversion;

interface RequestInterface
{
    public function get($url);

}