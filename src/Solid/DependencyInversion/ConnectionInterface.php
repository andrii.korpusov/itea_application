<?php

namespace AndriiKorpusov\IteaProject\Solid\DependencyInversion;

interface ConnectionInterface
{
    public function connect();
}