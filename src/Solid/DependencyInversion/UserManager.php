<?php
namespace AndriiKorpusov\IteaProject\Solid\DependencyInversion;

class UserManager
{
    private $http;

    private $logger;

    private $db;

    public function __construct(RequestInterface $http, LoggerInterface $logger, ConnectionInterface $db)
    {
        $this->http = $http;

        $this->logger = $logger;

        $this->db = $db;

    }

    public function getUser($url)
    {
        $this->logger->write('start execution');

        $user = $this->http->get($url);
        $this->logger->write('found user');
        $connection = $this->db->connect();



        $this->logger->write('start writing in db');
        mysqli_query($connection, 'INSERT');

        $userFromDb = mysqli_query($connection, 'SELECT');
        return $user;




    }
}