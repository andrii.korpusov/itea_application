<?php
namespace AndriiKorpusov\IteaProject\Solid\LiskovSubstitution;

class BaseUser
{
    protected $firstName = 'Andrii';

    protected $lastName = 'Korpusov';

    public function getFullName()
    {
        return sprintf('%s %s', $this->firstName, $this->lastName);
    }

    public function getDiscount()
    {
        return 5;
    }

}




