<?php
namespace AndriiKorpusov\Solid\OpenClose;

interface IShape {
    public function getArea();
}

class Square implements IShape {
    public $width;

    public function getArea()
    {
        return $this->width * $this->width;
    }
}

class Rectangle implements IShape {
    public $width;
    public $height;

    public function getArea()
    {
        return $this->width * $this->height;
    }

}

class Circle implements IShape {
    public $radius;

    public function getArea()
    {
        return pi() * pow($this->radius, 2);
    }
}



class ShapeManager {
    public function getArea(IShape $shape)
    {
        return $shape->getArea();
    }
}

