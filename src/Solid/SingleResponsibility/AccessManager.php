<?php

namespace AndriiKorpusov\Solid\SingleResponsibility;

class AccessManager
{
    private $userRepository;

    public function __construct(UserRepostiory $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function getAccessToModule($userId, $module)
    {
        $user = $this->userRepository->findUserById($userId);

        if ($user->getId() == $module->getUser()->getId()) {
            return true;
        }

        return false;
    }
}