<?php
namespace AndriiKorpusov\IteaProject\Models;


/**
 * Class User collect user info
 * properties
 * methods
 * @Class User
 */
class User
{
    /**
     * @var $username string
     * @ORM\Column(type="integer")
     */
    private $username;

    public function __construct()
    {

    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set username
     *
     * @Validate("string")
     * @param string $username
     * @return self
     */
    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Create User from HTTP request
     * {
     *      id: int,
     *      name: string,
     *      email: string
     * }
     * @Validate("int")
     * @param array $userData
     * @param $username
     * @return void
     */
    public function createUserFromRequest(array $userData, $username)
    {

    }

}