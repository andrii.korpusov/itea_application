<?php
namespace Class;

abstract class DBConnection
{
    public $host;
    public $dbname;
    public $username;
    public $password;
    public $port;

    public function __construct($host,$dbname,$username,$password,$port)
    {
        $this->host=$host;
        $this->dbname=$dbname;
        $this->username=$username;
        $this->password=$password;
        $this->port=$port;
    }

    public function connect()
    {
       $new_connection = new mysqli($this->host,$this->dbname,$this->username,$this->password,$this->port);
        if($new_connection->connect_error){
            die("Error: " . $new_connection->connect_error);
        } else {
            return $new_connection;
        }
    }

    public function addData($productId,$name,$price)
    {
        $connect = $this->connect();
        $query = 'INSERT INTO Order (name, productId, price) VALUES ("' . $name . '", "' .$productId . '", "' .$price . '")';
        mysqli_query($connect, $query);
        echo "Data is added";
        $connect->close();
    }
    public function removeData($productId)
    {
        $connect = $this->connect();
        $query = "DELETE FROM Order WHERE id = ".$productId;
        mysqli_query($connect, $query);
        echo "Data is remove";
        $connect->close();
    }
}

