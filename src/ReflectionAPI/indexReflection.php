<?php

$reflectionClass = new ReflectionClass(\AndriiKorpusov\IteaProject\Models\User::class);

$methods = get_class_methods(\AndriiKorpusov\IteaProject\Models\User::class);
$validators = [];

foreach ($methods as $method) {
    preg_match("/(Validate)\(\"\w+\"\)/", $reflectionClass->getMethod($method)->getDocComment(), $matches);
    if ($matches) {
        $start = strpos($matches[0], '"')+1;
        $end = strrpos($matches[0], '"');
        $type = substr($matches[0], $start, $end-$start);

        $validators[$method] = $type;
    }
}



$params = [
    'username' => 123,
    'createUserFromRequest' => 'username',
];

$user = new \AndriiKorpusov\IteaProject\Models\User();
try {

    foreach ($params as $property => $value) {
        $methodName = sprintf('set%s', ucfirst($property));
        if (isset($validators[$methodName])) {
            if (!call_user_func(sprintf('is_%s', $validators[$methodName]), $value)) {
                throw new Exception(sprintf('[%s]: Type of argument error: %s should be %s, %s given', __FILE__, $property, $validators[$methodName], gettype($value)));
            }

            $user->{$methodName}($value);
        }
    }
} catch (\Throwable $exception) {
    echo $exception->getMessage();
    die();
}
