<?php

namespace AndriiKorpusov\IteaProject\Client\Routes;

use AndriiKorpusov\IteaProject\Client\Controller\BlogController;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

class Routes
{
    public function getRoutes(): RouteCollection
    {
        $routes = new RouteCollection();
        $routes->add('find', new Route('/find', ['_controller'=>BlogController::class, '_method'=>'find']));
        $routes->add('find_article', new Route('/find/{id}', ['_controller'=>BlogController::class, '_method'=>'find']));
        $routes->add('create_article', new Route('/create', ['_controller'=>BlogController::class, '_method'=>'create']));

        return $routes;
    }
}