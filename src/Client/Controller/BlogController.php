<?php

namespace AndriiKorpusov\IteaProject\Client\Controller;

use AndriiKorpusov\IteaProject\Client\Service\RequestManager;
use GuzzleHttp\Client;

class BlogController
{
    private $requestManager;

    public function __construct()
    {
        $this->requestManager = new RequestManager();
    }

    public function create()
    {
        $article = [
            'title' => 'New article from client',
            'text' => 'How we created article',
            'userId' => 1,
        ];

        echo $this->requestManager->create($article);
    }

    public function find($id = '')
    {
        echo $this->requestManager->find($id);
    }

    public function delete()
    {

    }
}
