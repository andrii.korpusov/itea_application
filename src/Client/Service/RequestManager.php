<?php
namespace AndriiKorpusov\IteaProject\Client\Service;

use GuzzleHttp\Client;

class RequestManager
{
    private $guzzle;

    private $baseUrl = 'http://localhost';

    public function __construct()
    {
        $this->guzzle = new Client();
    }

    private function returnResponse($response)
    {
        if ($response->getStatusCode() === 200) {
            return $response->getBody();
        }

        return $response->getStatusCode();

    }

    public function find($id = '')
    {
        $URI = $id ? sprintf('/blog/articles/%s', $id) : '/blog/articles';
        $result = $this->guzzle->get(sprintf('%s%s', $this->baseUrl, $URI));

        return $this->returnResponse($result);
    }

    public function create($data)
    {
        $URI = '/blog/articles/create';

        $result = $this->guzzle->put(sprintf('%s%s', $this->baseUrl, $URI), [
            'json' => [
                'article' => $data
            ]
        ]);

        return $this->returnResponse($result);
    }
}