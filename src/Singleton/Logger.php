<?php
namespace AndriiKorpusov\IteaProject\Singleton;

class Logger
{
    private static $logger;

    private $name;

    private function __construct()
    {
    }

    private function __clone()
    {
    }

    public function __wakeup()
    {
        throw new \Exception('Cant wake up singleton');
    }

    public static function getInstance()
    {
        if (!self::$logger) {
            self::$logger = new self;
        }

        return self::$logger;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

}