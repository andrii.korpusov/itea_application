<?php
namespace App\Service;

use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;

class ProductManager
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function afterSale(Product $product, int $count)
    {
        $inStock = $product->getInStock();

        $product->setInStock($inStock - $count);

        return $product;
    }

    public function flush(Product $product)
    {
        $this->em->flush($product);
    }
}