<?php
namespace App\Command;

use App\Entity\Product;
use App\Service\ProductManager;
use App\Service\RemoteStockManager;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ProductUpdateStockCommand extends Command
{
    private $productManager;

    private $remoteStockManager;

    private $em;

    private $logger;

    public function __construct( EntityManagerInterface $em, ProductManager $productManager, RemoteStockManager $remoteStockManager, LoggerInterface $logger)
    {
        $this->logger = $logger;

        $this->productManager = $productManager;

        $this->remoteStockManager = $remoteStockManager;

        $this->em = $em;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('itea:product:update-stock');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $cnt = 10000;


        $price = 100;
        $category = 'category_1';
        for ($i = 1; $i < $cnt; $i++) {
            $product = new Product();

            $price = $i%200 ? $price : $price + 20;
            $category = $i%1000 ? $category : $category.$i;

            $product->setInStock($this->remoteStockManager->getStockCount($product))
                ->setName(sprintf('SomeProduct_%s', $i))
                ->setPrice($price)
                ->setStatus('active')
                ->setCategory($category);

            $this->em->persist($product);
        }


        $this->em->flush();

        $output->writeln('test update command');

        return Command::SUCCESS;
    }
}