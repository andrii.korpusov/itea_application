<?php
namespace App\EventListener;

use App\Entity\Order;
use App\Service\ProductManager;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;

class OrderListener
{
    private $productManager;

    public function __construct(ProductManager $productManager)
    {
        $this->productManager = $productManager;
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        if ($entity = $this->getObject($args)) {
            $this->productManager->afterSale($entity->getProduct(), 1);
        }
    }

    public function preUpdate(PreUpdateEventArgs $args)
    {

    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        if ($entity = $this->getObject($args)) {
            return;
        }


    }

    private function getObject($args)
    {
        $entity = $args->getObject();

        return $entity instanceof Order ? $entity : null;
    }

}