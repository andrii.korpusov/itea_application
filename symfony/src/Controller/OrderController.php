<?php

namespace App\Controller;

use App\Entity\Order;
use App\Form\OrderType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\MakerBundle\Validator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class OrderController extends AbstractController
{
    private $em;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * @Route("/order-create", "order_create")
     * @return void
     */
    public function createOrder(Request $request): Response
    {

        $order = new Order();

        $form = $this->createForm(OrderType::class, $order);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $order = $form->getData();
            $order->setProduct(3);
            try {
                $this->em->persist($order);

                $this->em->flush();

                return $this->redirect($this->generateUrl('order_list'));

            } catch (\Throwable $e) {
                dd($e);
            }


        }

        return $this->render('orderForm.html.twig', [
            'order' => $order,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/order-update/{order}", name="order_update")
     * @param Order $id
     * @return Response
     */
    public function updateOrder(Order $order, Request $request): Response
    {
        $form = $this->createForm(OrderType::class, $order);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $order = $form->getData();
            try {
                $this->em->persist($order);

                $this->em->flush();

                return $this->redirect($this->generateUrl('order_list'));

            } catch (\Throwable $e) {
                dd($e);
            }


        }

        return $this->render('orderForm.html.twig', [
            'order' => $order,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/order-list", name="order_list")
     * @return Response
     */
    public function actionList(): Response
    {
        $orderRepository = $this->em->getRepository(Order::class);

        $orders = $orderRepository->findAll();

        return $this->render('ordersList.html.twig', [
            'orders' => $orders
        ]);
    }

    /**
     * @Route("/order-show/{order}", name="order_show")
     * @param Order $order
     * @return Response
     */
    public function actionShow(Order $order): Response
    {

        return new Response($order->getId());
    }
}