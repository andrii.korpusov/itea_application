<?php
namespace App\Tests;

use App\Entity\Product;
use App\Service\ProductManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ProductManagerTest extends KernelTestCase
{
    public function testAfterSale()
    {
        self::bootKernel();

        $productManager = self::getContainer()->get(ProductManager::class);

        $product = new Product();

        $product->setName('test')
            ->setInStock(10);

        $productManager->afterSale($product, 1);

        $this->assertEquals(9, $product->getInStock());
    }
}