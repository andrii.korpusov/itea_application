<?php
session_start();
$config = [
    'hostname' => 'localhost',
    'user' => 'root',
    'password' => '',
    'db' => 'itea_shop'
];

$conn = mysqli_connect($config['hostname'], $config['user'], $config['password'], $config['db']);