<?php

require 'inc/db.php';



if (isset($_POST['_token']) && $_POST['_token'] === $_SESSION['_token']) {

    $sql = "UPDATE `user` SET status = 0 WHERE id = {$_SESSION['user_id']}";
    $result = mysqli_query($conn, $sql);
} else {
    exit(sprintf('error no token %s', $_POST['_token'] ?? '0'));
}


header('Location: /security/');