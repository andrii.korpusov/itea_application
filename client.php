<?php
require 'vendor/autoload.php';

use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\HttpFoundation\Request;
use AndriiKorpusov\IteaProject\Client\Routes\Routes;

$routes = (new Routes())->getRoutes();

$context = new RequestContext();
$context->fromRequest(Request::createFromGlobals());

$matcher = new UrlMatcher($routes, $context);

$parameters = $matcher->match($context->getPathInfo());

$result = [];

foreach ($parameters as $key => $parameter) {
    if ($key[0] !== '_') {
        $result[] = $parameter;
    }
}

$controller = new $parameters['_controller'];

$controller->{$parameters['_method']}(...$result);

