<?php

namespace IteaProject\Tests\Unit;

use AndriiKorpusov\IteaProject\Blog\Entity\Article;
use PHPUnit\Framework\TestCase;
use  AndriiKorpusov\IteaProject\Blog\Repository\Repository;
class RepositoryTest extends TestCase
{
    private $object;

    private $connection;

    protected function setUp()
    {
        $this->connection = $this->createMock(\PDO::class);

        $this->object = new Repository($this->connection);
    }

    public function testGetConnection()
    {
        $result = $this->object->getConnection();

        $this->assertEquals($this->connection, $result);
    }

    public function testDelete()
    {
        $id = 3;

        $pdoStatement = $this->createMock(\PDOStatement::class);

        $this->connection
            ->expects($this->once())->method('prepare')
            ->with($this->equalTo('DELETE FROM article WHERE id=:id'))
            ->willReturn($pdoStatement);


        $pdoStatement->expects($this->once())->method('bindParam')
            ->with('id', $id)
            ->willReturn(true);

        $pdoStatement->expects($this->once())->method('execute')
            ->willReturn(true);

        $article = new Article();
        $article->setId($id);

        $result = $this->object->delete($article);

        $this->assertTrue($result);
    }

}