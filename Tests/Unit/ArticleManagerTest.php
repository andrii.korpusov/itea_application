<?php

namespace IteaProject\Tests\Unit;

use AndriiKorpusov\IteaProject\Blog\Entity\Article;
use AndriiKorpusov\IteaProject\Blog\Repository\Repository;
use AndriiKorpusov\IteaProject\Blog\Service\ArticleManager;
use AndriiKorpusov\IteaProject\Blog\Transformer\ArticleTransformer;
use PHPUnit\Framework\TestCase;

class ArticleManagerTest extends TestCase
{
    private $object;

    private $repository;

    private $connection;

    protected function setUp()
    {
        $this->connection = $this->createMock(\PDO::class);

        $this->repository = new Repository($this->connection);

        $this->object = new ArticleManager($this->repository, new ArticleTransformer());
    }

    public function testCreate()
    {
        $data = [
            'title' => 'title',
            'text' => 'text',
            'userId' => 'userId'
        ];

        $pdoStatement = $this->createMock(\PDOStatement::class);

        $this->connection->expects($this->once())->method('prepare')
            ->willReturn($pdoStatement);

        $pdoStatement->expects($this->any())->method('bindParam')
            ->willReturn(true);


        $result = $this->object->create((object)$data);

        $this->assertInstanceOf(Article::class, $result);
    }
}