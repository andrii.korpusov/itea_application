<?php

namespace IteaProject\Tests\Unit;

use AndriiKorpusov\IteaProject\Blog\Entity\Article;
use AndriiKorpusov\IteaProject\Blog\Entity\Tag;
use AndriiKorpusov\IteaProject\Blog\Service\DataMapper;
use PHPUnit\Framework\TestCase;

class DataMapperTest extends TestCase
{
    private $object;

    protected function setUp()
    {
        $this->article = $this->createMock(Article::class);
        $this->object = new DataMapper($this->article);
    }

    public function testGetDataFromObject()
    {

    }

    public function testGetObjectFromData()
    {

        $testData = [
            'title' => 'title'
        ];

        $this->article->expects($this->once())->method('setTitle')
            ->with('title')
            ->willReturn($this->article);

        $result = $this->object->getObjectFromData($testData);


        $this->assertInstanceOf(Article::class, $result);

//        $this->assertEquals('title', $result->getTitle());

    }

}