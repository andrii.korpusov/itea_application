<?php
require __DIR__.'/../../vendor/autoload.php';
require __DIR__.'/ContainerBuilder.php';

$container = require sprintf('%s/../../bootstrap/container.php', __DIR__);

\IteaProject\Tests\Integration\ContainerBuilder::setContainer($container);

