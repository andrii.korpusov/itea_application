<?php

namespace IteaProject\Tests\Integration;

use PHPUnit\Framework\TestCase;

class ArticleManagerTest extends TestCase
{
    private $container;

    private $articleManager;

    protected function setUp()
    {

        $this->container = ContainerBuilder::getContainer();
dd($this->container);exit;
        $this->articleManager = $this->container->get('ArticleManager');
    }

    public function testCreate()
    {
        $article = (object) [
            'title' => 'testTitle',
            'text' => 'testText',
            'userId' => 1
        ];

        $savedArticle = $this->articleManager->create($article);

        $this->assertNotNull($savedArticle->getId());

    }
}