<?php

namespace IteaProject\Tests\Integration;


class ContainerBuilder
{
    private static $container;

    /**
     * @return mixed
     */
    public static function getContainer()
    {
        return self::$container;
    }

    /**
     * @param mixed $container
     */
    public static function setContainer($container)
    {
        self::$container = $container;
    }


}