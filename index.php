<?php
require 'vendor/autoload.php';

use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\HttpFoundation\Request;
use AndriiKorpusov\IteaProject\Blog\Routes\Routes;



$container = require sprintf('%s/bootstrap/container.php', __DIR__);

$routes = (new Routes())->getRoutes();

$context = new RequestContext();
$context->fromRequest(Request::createFromGlobals());

$matcher = new UrlMatcher($routes, $context);

$parameters = $matcher->match($context->getPathInfo());

$result = [];

foreach ($parameters as $key => $parameter) {
    if ($key[0] !== '_') {
        $result[] = $parameter;
    }
}

$controller = $container->get($parameters['_controller']);

echo $controller->{$parameters['_method']}(...$result);



#autowire
#php config
#annotations doctrine\annotations



/*
//Chain of responsibility
//Cтворюємо екземпляр класу який буде викликати наші ланки при перевірці даних
$server = new Server();

//створюємо ланки
$chain = new FirstChain();
//встановлюємо наступні обробники, їх може бути безліч і можна їх передавати в будь якому порядку
$chain->setNext(new SecondChain())
    ->setNext(new ThirdChain());

//встановлюємо обробники до нашого "серверу"
$server->setChain($chain);

do {
    //викликається перший обробник, метод який і буде запускати перевірки
    $success = $server->runCheck(2);

} while(!$success);
//Виводимо назву класу на якому зупинилась перевірка
//Таким чином можна перевіряти наприклад рівні доступів, першою ланкою перевіряєм чи юзер == Адмін,
//І далі по спаданню видаючи потрібні доступи для кожної ролі
echo $success;


require 'vendor/autoload.php';

use AndriiKorpusov\IteaProject\Cor\FirstChain;
use AndriiKorpusov\IteaProject\Cor\SecondChain;
use AndriiKorpusov\IteaProject\Cor\ThirdChain;
use AndriiKorpusov\IteaProject\Cor\Server;
use AndriiKorpusov\IteaProject\Factory\TicketFinder;
use AndriiKorpusov\IteaProject\Factory\OperaFinder;

//Chain of responsibility
//Cтворюємо екземпляр класу який буде викликати наші ланки при перевірці даних
$server = new Server();

//створюємо ланки
$chain = new FirstChain();
//встановлюємо наступні обробники, їх може бути безліч і можна їх передавати в будь якому порядку
$chain->setNext(new SecondChain())
    ->setNext(new ThirdChain());
echo "<br/>";
//Factory
//Метод в який ми передаємо наш абстрактний клас для пошуку квитків
function findTicketPlaces(TicketFinder $ticketFinder) {
    return $ticketFinder->getTicketPlaces();
}
$json = '';
for ($i = 1; $i<4; $i++) {

//встановлюємо обробники до нашого "серверу"
$server->setChain($chain);

do {
    //викликається перший обробник, метод який і буде запускати перевірки
    $success = $server->runCheck(2);

} while(!$success);
//Виводимо назву класу на якому зупинилась перевірка
//Таким чином можна перевіряти наприклад рівні доступів, першою ланкою перевіряєм чи юзер == Адмін,
//І далі по спаданню видаючи потрібні доступи для кожної ролі
echo $success;
//Ми знаємо що наш захід відбувається в опері - ми шукаємо квитки, передаємо клас OperaFinder у функцію
$freePlaces = findTicketPlaces(new OperaFinder());
    $json .= ($json == '' ? '[':',') . file_get_contents(sprintf('https://jsonplaceholder.typicode.com/todos/%s', $i));
}
$json .= ']';

$logger = new \Monolog\Logger('iteaLogger');
$logger->pushHandler(new \Monolog\Handler\StreamHandler('itea.log', \Psr\Log\LogLevel::WARNING));

$csvCreator = new CSVCreator($logger, __DIR__. '/csv/');
var_dump($freePlaces);
*/
//
//$config = [
//    'user' => 'root',
//    'pass' => '',
//    'db' => 'itea_project'
//];
//
//$userRepository = new UserRepository($config, User::class);
//
//$data = [
//    'firstName' => 'Andrii',
//    'lastName' => 'Korpusov',
//    'email' => 'andrii.korpusov@gmail.com',
//    'password' => '123321',
//    'role' => 'ROLE_ADMIN'
//];
//
//$userManager = new \AndriiKorpusov\IteaProject\Blog\Service\UserManager();
//
//$user = $userManager->create($data);
//
//
//$userRepository->insert($user);

/*
 * RESTful  Principles
 *
 * 1. Stateless  Незалежність від стану
 *
 * Сервер не має зберігати інформацію про клієнта
 * Клієнт відправляє інформацію про себе з кожним запитом
 *
 * token - зберігається на стороні клієнта
 * baseAuth - username password
 *
 * 2. Багаторівнева архітектура
 *
 * Клієнт не знає endpoint server
 * Сервер - файлове сховище -в базу даних -під'єднується до стороннього ресурсу
 *
 * 3. Єдиний уніфікований програмний інтерфейс URI
 *
 * /blog/articles - all articles
 * /blog/articles/create - create article
 * /blog/articles/delete - delete article
 *
 * /blog/tags
 * /blog/tags/create
 * /blog/tags/delete
 *
 * 4. Cache - кешування
 * /blog/articles
 * кеш налаштовується lifetime
 * кешування на визначений час
 *
 *
 * 5. Зручне подання даних
 * {
 *  "id" : 1,
 *  "firstName": "Andrii"
 *  "lastName": "Korpusov"
 *  "email": "andrii.korpusov@gmail.com"
 *  "role": "ROLE_ADMIN"
 * }
 *
 *
 * JSON XML
 *
 *
 *
 * GET - отримання даних
 * POST - створення
 * PUT - оновлення
 * PATCH - часткове оновлення
 * DELETE
 *
 * PUT
 * {
 *  "id":1,
 *  "name:"Andrii"
 *  "lastName": "Korpusov"
 *  "email": "andrii.korpusov@itea.com"
 *  "role": "ROLE_ADMIN"
 * }
 *
 * PATCH
 * {
 *  "id":1,
 *  "email": "andrii.korpusov@itea.com"
 * }
 *
 * ІДЕМПОТЕНТНІСТЬ
 *
 * GET
 * PUT
 * PATCH
 * DELETE
 *
 *
 * POST
 * {
 *  "firstName": "Andrii",
 *  "lastName": "Korpusov",
 *  "email": "andrii.korpusov@gmail.com",
 *  "role": "ROLE_ADMIN"
 * }
 *
 * RESPONSE CODE
 *
 * 1xx - info
 * 2xx - success 200/201 created/204 changed
 * 3xx - 302 moved / 301 redicted
 * 4xx - 404/401 not auth/403 forbidden - errors client side
 * 5xx - 500 internal server error 502 bad gateway
 *
 *
 * HATEOAS
 * Hypermedia as the engine of application state
 * гіпермедіа як двигун стану додатку
 *
 * URI
 * https://itea_project.com
 *
 * /blogs/articles/1
 * /blogs/articles/delete
 *
 * {
 *  "id":1,
 *  "title": "FirstArticle",
 *  "text": "Very long and interested text",
 *  "userId": 1
 * }
 *
 * {
 *  "data": {
 *      "id":1,
 *      "attributes": {
 *          "title": "FirstArticle",
 *          "text": "Very long and interested text",
 *          "userId": 1
 *      }
 *
 *  },
 *  "links": {
 *      "self": {"url": "https://itea_project.com/blogs/articles/1"},
 *      "update": {"url": "https://itea_project.com/blogs/articles/1/update"},
 *      "delete": {"url": "https://itea_project.com/blogs/articles/1/delete"}
 *  }
 * }
 * стандарт HAL - hypertext application language
 *
 * PATCH https://itea_project.com/blogs/articles/1/update
 *
 * {
 *  "data" : {
 *      "id":1,
 *      "attributes": {
 *          "title" : "ChangedTitle"
 *      }
 *    }
 * }
 *
 *
 *
 * JSON:API specification
 * Визначає як клієнт має звертатися до сервера, запитувати ресурси, модифікувати ресурси
 *
 * Як сервер має віддавати ресурси
 *
 * MUST HAVE at least one
 * {
 *  "data" : {
*      type:article
 *      relationships: {
 *          author: {
 * }
 *  }
 *   },
 *  "errors",
 *  "meta" : {
 *      "Copyright": "",
*       "author":""
 *  }
 * }
 *
 * MUST NOT contain data and error in one response
 *
 *
 * MAY contains
 *
 * {
 *  "links",
 *  "included":
 * }
 *
 * немає data - included теж немає бути
 *
 * {
 *  links: {
 *      self: link to self resource,
 *      related: якщо це в relationships
 *      pagination: {
 *          first:
 *          last:
*           next:
 *          prev:
 * }
 * }
 * }
 *
 * data: {
 *  type: article
 *  id: 2
 *  attributes:
 *  relationships
 *  links
 * }
 *
 * data: [
 * {
 *  type:article,
 *  id: 1
 * },
 * {
 *  type: article,
 *  id: 2
 * }
 * ]
 *
 * data: null
 *{
 * data {
 *  "type": "article",
 *  "id": "1",
 *  "attributes": {
 *      "title": "Title",
 *      "text": "text of article",
 *      "active": "true|false",
 *      "createAt": "2022-07-18"
 *  },
 *  relationships: {
 *      "author": {
 *          data: {
 *              "type": "user",
 *              "id": "1"
 *          },
 *          links: {
 *              "self": "/blog/articles/1",
 *              "related": "/user/1"
 *          }
 *      },
 *      "tags": [{
 *          "data": {
 *              "type":"tag",
 *              "id" : "1"
 *          }
 *      }]
 *  }
 * },
 * include: [{
 *    {
 *          "type":"user",
 *          "id":"1",
 *          "attributes": {
 *              "firstName": "Andrii",
 *              "lastName": "Korpusov"
 *          }
 *
 *      }
*    {
 *      "type": "tag",
*       "id": "1",
*       "attributes": {
 *          "name": "news",
 *          "active":"true"
 *      }
 * }]
 *
 * }
 *
 * a-z A-Z 0-9 - _
 *
 * + ? , [ ] #$%
 *
 *
 * relationships:
 *
 * MUST HAVE at least one
 * links
 * data
 * meta
 *
 */